# Translation of plasma_applet_org.kde.plasma.kicker to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2014.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-09 00:39+0000\n"
"PO-Revision-Date: 2018-06-24 21:56+0100\n"
"Last-Translator: Øystein Steffensen-Alværvik <ystein@posteo.net>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Generelt"

#: package/contents/ui/code/tools.js:44
#, kde-format
msgid "Remove from Favorites"
msgstr "Fjern fra favoritter"

#: package/contents/ui/code/tools.js:48
#, kde-format
msgid "Add to Favorites"
msgstr "Legg til i favoritter"

#: package/contents/ui/code/tools.js:72
#, kde-format
msgid "On All Activities"
msgstr "På alle aktiviteter"

#: package/contents/ui/code/tools.js:122
#, kde-format
msgid "On the Current Activity"
msgstr ""

#: package/contents/ui/code/tools.js:136
#, kde-format
msgid "Show in Favorites"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Icon:"
msgstr "Ikon:"

#: package/contents/ui/ConfigGeneral.qml:127
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:132
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Fjern ikon"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Show applications as:"
msgstr "Vis programmer som:"

#: package/contents/ui/ConfigGeneral.qml:152
#, kde-format
msgid "Name only"
msgstr "Bare navn"

#: package/contents/ui/ConfigGeneral.qml:152
#, kde-format
msgid "Description only"
msgstr "Bare beskrivelse"

#: package/contents/ui/ConfigGeneral.qml:152
#, kde-format
msgid "Name (Description)"
msgstr "Navn (beskrivelse)"

#: package/contents/ui/ConfigGeneral.qml:152
#, kde-format
msgid "Description (Name)"
msgstr "Beskrivelse (navn)"

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgid "Behavior:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:164
#, kde-format
msgid "Sort applications alphabetically"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:172
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:180
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:190
#, kde-format
msgid "Show categories:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:193
#, kde-format
msgid "Recent applications"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:194
#, kde-format
msgid "Often used applications"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:201
#, kde-format
msgid "Recent files"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:202
#, kde-format
msgid "Often used files"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:208
#, kde-format
msgid "Sort items in categories by:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:209
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:209
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:219
#, kde-format
msgid "Search:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:221
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "Utvid søket til bokmerker, filer og e-poster"

#: package/contents/ui/ConfigGeneral.qml:229
#, kde-format
msgid "Align search results to bottom"
msgstr "Plasser søkeresultater nederst"

#: package/contents/ui/DashboardRepresentation.qml:235
#, kde-format
msgid "Searching for '%1'"
msgstr "Søker etter «%1»"

#: package/contents/ui/DashboardRepresentation.qml:235
#, kde-format
msgctxt "@info:placeholder as in, 'start typing to initiate a search'"
msgid "Type to search…"
msgstr ""

#: package/contents/ui/DashboardRepresentation.qml:331
#, kde-format
msgid "Favorites"
msgstr "Favoritter"

#: package/contents/ui/main.qml:243
#, kde-format
msgid "Edit Applications…"
msgstr ""

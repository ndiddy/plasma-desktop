# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2014, 2015, 2016, 2018, 2020, 2023.
# Lukáš Tinkl <ltinkl@redhat.com>, 2010.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-22 00:39+0000\n"
"PO-Revision-Date: 2023-02-22 16:43+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Lukáš Tinkl,Lukáš Kucharczyk"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukas@kde.org,lukas@kucharczyk.xyz"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""
"Získávání informací o vstupních zařízeních se nezdařilo. Znovu otevřete "
"tento modul nastavení."

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""
"Při načítání základních informací o zařízení %1 došlo ke kritické chybě."

#: kcm/libinput/libinput_config.cpp:100
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""
"Chyba při načítání hodnot.Více informací naleznete v záznamech činnosti. "
"Prosím restartujte tento nastavovací modul."

#: kcm/libinput/libinput_config.cpp:105
#, kde-format
msgid "No pointer device found. Connect now."
msgstr "Žádné ukazovací zařízení nebylo nalezeno. Připojte zařízení."

#: kcm/libinput/libinput_config.cpp:116
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""
"Nebylo možné uložit všechny změny. Více informací naleznete v záznamu. "
"Restartujte tento konfigurační modul a zkuste to znovu."

#: kcm/libinput/libinput_config.cpp:136
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""
"Chyba při načítání výchozích hodnot. Nastavení některých voleb na výchozí "
"hodnotu selhalo."

#: kcm/libinput/libinput_config.cpp:158
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""
"Při přidávání nově připojeného zařízení došlo k chybě. Připojte zařízení "
"znovu a restartujte tento konfigurační modul."

#: kcm/libinput/libinput_config.cpp:182
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr "Ukazovací zařízení bylo odpojeno. Zavřete dialogové okno nastavení."

#: kcm/libinput/libinput_config.cpp:184
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""
"Ukazovací zařízení bylo odpojeno. Žádná další zařízení nebyla nalezena."

#: kcm/libinput/main.qml:75
#, kde-format
msgid "Device:"
msgstr "Zařízení:"

#: kcm/libinput/main.qml:99 kcm/libinput/main_deviceless.qml:50
#, kde-format
msgid "General:"
msgstr "Obecné:"

#: kcm/libinput/main.qml:100
#, kde-format
msgid "Device enabled"
msgstr "Zařízení je povolené"

#: kcm/libinput/main.qml:120
#, kde-format
msgid "Accept input through this device."
msgstr "Povolit vstup skrze toto zařízení."

#: kcm/libinput/main.qml:125 kcm/libinput/main_deviceless.qml:51
#, kde-format
msgid "Left handed mode"
msgstr "Režim pro leváka"

#: kcm/libinput/main.qml:145 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr "Prohodit levé a pravé tlačítko."

#: kcm/libinput/main.qml:152 kcm/libinput/main_deviceless.qml:78
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr "Levé a pravé tlačítko je prostřední tlačítko"

#: kcm/libinput/main.qml:172 kcm/libinput/main_deviceless.qml:98
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""
"Kliknutí levým a pravým tlačítkem zároveň je kliknutí prostředním tlačítkem."

#: kcm/libinput/main.qml:176 kcm/libinput/main_deviceless.qml:101
#, kde-format
msgid ""
"Activating this setting increases mouse click latency by 50ms. The extra "
"delay is needed to correctly detect simultaneous left and right mouse clicks."
msgstr ""

#: kcm/libinput/main.qml:186 kcm/libinput/main_deviceless.qml:112
#, kde-format
msgid "Pointer speed:"
msgstr "Rychlost ukazatele:"

#: kcm/libinput/main.qml:285 kcm/libinput/main_deviceless.qml:144
#, kde-format
msgid "Pointer acceleration:"
msgstr "Zrychlení ukazatele:"

# žádné parametry funkce v inspektoru funkcí
#: kcm/libinput/main.qml:316 kcm/libinput/main_deviceless.qml:175
#, kde-format
msgid "None"
msgstr "Nic"

#: kcm/libinput/main.qml:320 kcm/libinput/main_deviceless.qml:179
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr "Kurzor se pohybuje o stejnou vzdálenost jako myš."

#: kcm/libinput/main.qml:326 kcm/libinput/main_deviceless.qml:185
#, kde-format
msgid "Standard"
msgstr "Standardní"

#: kcm/libinput/main.qml:330 kcm/libinput/main_deviceless.qml:189
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr "Kurzor se pohybuje v závislosti na rychlosti pohybu myši."

#: kcm/libinput/main.qml:342 kcm/libinput/main_deviceless.qml:201
#, kde-format
msgid "Scrolling:"
msgstr "Rolování:"

#: kcm/libinput/main.qml:343 kcm/libinput/main_deviceless.qml:202
#, kde-format
msgid "Invert scroll direction"
msgstr "Obrátit směr rolování"

#: kcm/libinput/main.qml:359 kcm/libinput/main_deviceless.qml:218
#, kde-format
msgid "Touchscreen like scrolling."
msgstr "Rolování jako na dotykové obrazovce."

#: kcm/libinput/main.qml:364
#, kde-format
msgid "Scrolling speed:"
msgstr "Rychlost rolování:"

#: kcm/libinput/main.qml:414
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr "Pomaleji"

#: kcm/libinput/main.qml:421
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr "Rychleji"

#: kcm/libinput/main.qml:432
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr "Znovu připojit dodatečná tlačítka myši..."

#: kcm/libinput/main.qml:470
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr "Extra tlačítko %1:"

#: kcm/libinput/main.qml:500
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr "Stiskněte tlačítko pro které chcete přiřadit klávesovou zkratku..."

#: kcm/libinput/main.qml:501
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr "Zadejte novou kombinaci pro %1"

#: kcm/libinput/main.qml:505
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Zrušit"

#: kcm/libinput/main.qml:522
#, kde-format
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Stiskněte tlačítko myši"

#: kcm/libinput/main.qml:523
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr "Přidat zkratku..."

#: kcm/libinput/main.qml:552
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr "Přejít zpět"

# translation of kcmkonq.po to Slovenian
# Translation of kcmkonq.po to Slovenian
# SLOVENIAN TRANSLATION OF KCMKONQ.
# $Id: kcm_desktoppaths.po 1662170 2023-10-12 02:05:01Z scripty $
# $Source$
#
# Copyright (C) 2001, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.
# Roman Maurer <roman.maurer@amis.net>, 2001.
# Gregor Rakar <gregor.rakar@kiss.si>, 2003, 2004, 2005.
# Jure Repinc <jlp@holodeck1.com>, 2006, 2007, 2008, 2009.
# Andrej Mernik <andrejm@ubuntu.si>, 2013, 2018.
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcmkonq\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-12 01:43+0000\n"
"PO-Revision-Date: 2023-02-07 08:36+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 22.12.2\n"

#: ui/main.qml:52
#, kde-format
msgid "Desktop path:"
msgstr "Pot do namizja:"

#: ui/main.qml:58
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"V tej mapi so vse datoteke, ki jih vidite na namizju. Če želite, lahko "
"spremenite njeno mesto in tja se bo samodejno prestavila tudi vsebina."

#: ui/main.qml:66
#, kde-format
msgid "Documents path:"
msgstr "Pot do dokumentov:"

#: ui/main.qml:72
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr "To bo privzeta mapa za nalaganje in shranjevanje dokumentov."

#: ui/main.qml:80
#, kde-format
msgid "Downloads path:"
msgstr "Pot do prejemov:"

#: ui/main.qml:86
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr "To bo privzeta mapa za shranjevanje prejetih datotek."

#: ui/main.qml:94
#, kde-format
msgid "Videos path:"
msgstr "Pot do videov:"

#: ui/main.qml:100 ui/main.qml:142
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr "To bo privzeta mapa za nalaganje in shranjevanje filmov."

#: ui/main.qml:108
#, kde-format
msgid "Pictures path:"
msgstr "Pot do slik:"

#: ui/main.qml:114
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr "To bo privzeta mapa za nalaganje in shranjevanje slik."

#: ui/main.qml:122
#, kde-format
msgid "Music path:"
msgstr "Pot do glasbe:"

#: ui/main.qml:128
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr "To bo privzeta mapa za nalaganje in shranjevanje glasbe."

#: ui/main.qml:136
#, kde-format
msgid "Public path:"
msgstr "Javna pot:"

#: ui/main.qml:150
#, kde-format
msgid "Templates path:"
msgstr "Pot do predlog:"

#: ui/main.qml:156
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr "To bo privzeta mapa za nalaganje in shranjevanje predlog."

#: ui/UrlRequester.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr "Izberite novo lokacijo"

#~ msgid "Desktop"
#~ msgstr "Namizje"

#~ msgid "Documents"
#~ msgstr "Dokumenti"

#~ msgid "Downloads"
#~ msgstr "Prejemi"

#~ msgid "Music"
#~ msgstr "Glasba"

#~ msgid "Pictures"
#~ msgstr "Slike"

#~ msgid "Videos"
#~ msgstr "Videi"

#~ msgid "Public"
#~ msgstr "Javno"

#~ msgid "Templates"
#~ msgstr "Predloge"

#~ msgid ""
#~ "<h1>Paths</h1>\n"
#~ "This module allows you to choose where in the filesystem the files on "
#~ "your desktop should be stored.\n"
#~ "Use the \"Whats This?\" (Shift+F1) to get help on specific options."
#~ msgstr ""
#~ "<h1>Poti</h1>\n"
#~ "Ta modul vam omogoča izbrati, kje v vašem datotečnem sistemu so shranjene "
#~ "datoteke na vašem namizju.\n"
#~ "Uporabite »Kaj je to?« (Shift+F1) za pomoč pri posameznih možnostih."

#~ msgid ""
#~ "This folder will be used by default to load or save public shares from or "
#~ "to."
#~ msgstr ""
#~ "To bo privzeta mapa za nalaganje in shranjevanje javnih deljenih datotek."

#~ msgid "Autostart path:"
#~ msgstr "Pot do samodejnega zagona:"

#~ msgid ""
#~ "This folder contains applications or links to applications (shortcuts) "
#~ "that you want to have started automatically whenever the session starts. "
#~ "You can change the location of this folder if you want to, and the "
#~ "contents will move automatically to the new location as well."
#~ msgstr ""
#~ "V tej mapi so programi ali povezave do njih (bližnjice), ki se samodejno "
#~ "zaženejo vsakič, ko se zažene seja. Če želite, lahko spremenite njeno "
#~ "mesto in tja se bo samodejno prestavila tudi vsebina."

#~ msgid "Autostart"
#~ msgstr "Samodejni zagon"

#~ msgid "Movies"
#~ msgstr "Filmi"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want the files to be moved from '%2' to '%3'?"
#~ msgstr ""
#~ "Pot za »%1« je bila spremenjena.\n"
#~ "Ali želite, da se datoteke iz »%2« premaknejo v »%3«?"

#~ msgctxt "Move files from old to new place"
#~ msgid "Move"
#~ msgstr "Premakni"

#~ msgctxt "Use the new directory but do not move files"
#~ msgid "Do not Move"
#~ msgstr "Ne premakni"

#~ msgid ""
#~ "The path for '%1' has been changed.\n"
#~ "Do you want to move the directory '%2' to '%3'?"
#~ msgstr ""
#~ "Pot za »%1« je bila spremenjena.\n"
#~ "Ali želite, da se mapo iz »%2« premakne v »%3«?"

#~ msgctxt "Move the directory"
#~ msgid "Move"
#~ msgstr "Premakni"

#~ msgctxt "Use the new directory but do not move anything"
#~ msgid "Do not Move"
#~ msgstr "Ne premakni"

#~ msgid "Confirmation Required"
#~ msgstr "Zahtevana je potrditev"

# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020, 2021, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-12 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 08:29+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: globalaccelmodel.cpp:235
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Error durante que il salveguardava %1: %2"

#: globalaccelmodel.cpp:347
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Error durante que on addeva %1, il appare que il ha necun actiones."

#: globalaccelmodel.cpp:397
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Error durante que il communicava con le servicio de vias breve global"

#: kcm_keys.cpp:57
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Il falleva a communicar con le demone de vias breve global"

#: kcm_keys.cpp:312
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Via breve %1 es ja assignate al action commun %2 '%3'.\n"
"Tu vole reassignar lo?"

#: kcm_keys.cpp:316
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Via breve %1 es ja assignate al action commun ?%2' de  %3 \n"
"Tu vole reassignar lo?"

#: kcm_keys.cpp:317
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Trovate conflicto"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "File"
msgstr "File"

#: standardshortcutsmodel.cpp:35
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "Navigation"
msgstr "Navigation"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "View"
msgstr "Vista"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Settings"
msgstr "Preferentias"

#: standardshortcutsmodel.cpp:40
#, kde-format
msgid "Help"
msgstr "Adjuta"

#: ui/main.qml:27
#, kde-format
msgid "Applications"
msgstr "Applicationes"

#: ui/main.qml:27
#, kde-format
msgid "Commands"
msgstr "Commandos"

#: ui/main.qml:27
#, kde-format
msgid "System Services"
msgstr "Preferentias de systema"

#: ui/main.qml:27
#, kde-format
msgid "Common Actions"
msgstr "Actiones commun"

#: ui/main.qml:43
#, kde-format
msgctxt "@action: button Import shortcut scheme"
msgid "Import…"
msgstr "Importa…"

#: ui/main.qml:48
#, kde-format
msgctxt "@action:button"
msgid "Cancel Export"
msgstr "Cancella exportation"

#: ui/main.qml:49
#, kde-format
msgctxt "@action:button Export shortcut scheme"
msgid "Export…"
msgstr "Exporta…"

#: ui/main.qml:75
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Non pote exportar schema quando on ha modificationes non salveguardate"

#: ui/main.qml:87
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"Selige le componentes a basso que deberea esser includite in le schema "
"exportate"

#: ui/main.qml:93
#, kde-format
msgctxt "@action:button Save shortcut scheme"
msgid "Save Scheme"
msgstr "Salveguarda schema"

#: ui/main.qml:158
#, kde-format
msgctxt "@action:button Add new shortcut"
msgid "Add New"
msgstr "Adde nove"

#: ui/main.qml:164
#, kde-format
msgctxt "@action:menu End of the sentence 'Add New Application…'"
msgid "Application…"
msgstr "Application…"

#: ui/main.qml:170
#, kde-format
msgctxt "@action:menu End of the sentence 'Add New Command or Script…'"
msgid "Command or Script…"
msgstr "Commando o script"

#: ui/main.qml:226
#, kde-format
msgctxt "@tooltip:button %1 is the text of a custom command"
msgid "Edit command for %1"
msgstr "Edita commando per %1"

#: ui/main.qml:242
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Remover omne vias breve per %1"

#: ui/main.qml:253
#, kde-format
msgid "Undo deletion"
msgstr "Annulla deletion"

#: ui/main.qml:307
#, kde-format
msgid "No items matched the search terms"
msgstr "Necun terminos coincideva con le terminos de cerca"

#: ui/main.qml:340
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Seliger un elemento ex le lista de vider su vias breve hic"

#: ui/main.qml:352
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Exportar schema de via breve"

#: ui/main.qml:352 ui/main.qml:470
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Importar schema de via breve"

#: ui/main.qml:354
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Schema de via breve (*.kksrc)"

#: ui/main.qml:382
#, kde-format
msgid "Edit Command"
msgstr "Commando de edit"

#: ui/main.qml:382
#, kde-format
msgid "Add Command"
msgstr "Adde Commando"

#: ui/main.qml:400
#, kde-format
msgid "Save"
msgstr "Salveguarda"

#: ui/main.qml:400
#, kde-format
msgid "Add"
msgstr "Adde"

#: ui/main.qml:426
#, kde-format
msgid "Enter a command or choose a script file:"
msgstr "Inserta un commando o selige un file de script:"

#: ui/main.qml:441
#, kde-format
msgctxt "@action:button"
msgid "Choose…"
msgstr "Selige..."

#: ui/main.qml:454
#, kde-format
msgctxt "@title:window"
msgid "Choose Script File"
msgstr "Selige file de script"

#: ui/main.qml:456
#, kde-format
msgctxt "Template for file dialog"
msgid "Script file (*.*sh)"
msgstr "File de Script  (*.*sh)"

#: ui/main.qml:477
#, kde-format
msgid "Select the scheme to import:"
msgstr "Selige le schema de importar:"

#: ui/main.qml:495
#, kde-format
msgid "Custom Scheme"
msgstr "Personalisar schema"

#: ui/main.qml:500
#, kde-format
msgid "Select File…"
msgstr "Selige File…"

#: ui/main.qml:500
#, kde-format
msgid "Import"
msgstr "Importa"

#: ui/ShortcutActionDelegate.qml:34
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Editante via breve: %1"

#: ui/ShortcutActionDelegate.qml:53
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: ui/ShortcutActionDelegate.qml:77
#, kde-format
msgid "No active shortcuts"
msgstr "Necun vias breve active"

#: ui/ShortcutActionDelegate.qml:120
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Via breve predefinite"
msgstr[1] "Vias breve predefinite"

#: ui/ShortcutActionDelegate.qml:122
#, kde-format
msgid "No default shortcuts"
msgstr "Nulle vias breve predefinite"

#: ui/ShortcutActionDelegate.qml:131
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Via breve predefinite %1 es habilitate."

#: ui/ShortcutActionDelegate.qml:131
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Via breve predefinite %1 es dishabilitate."

#: ui/ShortcutActionDelegate.qml:154
#, kde-format
msgid "Custom shortcuts"
msgstr "Vias breve adaptabile"

#: ui/ShortcutActionDelegate.qml:183
#, kde-format
msgid "Delete this shortcut"
msgstr "Deler iste via breve"

#: ui/ShortcutActionDelegate.qml:189
#, kde-format
msgid "Add custom shortcut"
msgstr "Adder via breve personalisate"

#: ui/ShortcutActionDelegate.qml:227
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Cancellar le captura de nove via breve"

#~ msgctxt "@action:button Keep translated text as short as possible"
#~ msgid "Add Command…"
#~ msgstr "Adde Commando…"

#~ msgid "Import Scheme…"
#~ msgstr "Importa schema..."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "Shortcuts"
#~ msgstr "Vias breve"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"

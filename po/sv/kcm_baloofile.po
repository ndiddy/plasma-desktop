# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2021-05-27 18:53+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: ui/main.qml:44
#, kde-format
msgid "Pause Indexer"
msgstr "Pausa indexering"

#: ui/main.qml:44
#, kde-format
msgid "Resume Indexer"
msgstr "Återuppta indexering"

#: ui/main.qml:72
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""
"Inaktiverar filsökning i Kör program och startmenyer, och tar bort visning "
"av utökad metadata från alla KDE-program."

#: ui/main.qml:81
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""
"Vill du ta bort sparad indexerad data? Utrymmet %1 kommer att frigöras, men "
"om indexering aktiveras igen senare, måste hela indexet skapas om från "
"början. Det kan ta en stund, beroende på hur många filer du har."

#: ui/main.qml:83
#, kde-format
msgid "Delete Index Data"
msgstr "Ta bort indexerad data"

#: ui/main.qml:93
#, fuzzy, kde-format
#| msgid ""
#| "File Search helps you quickly locate all your files based on their "
#| "content."
msgid ""
"File Search helps you quickly locate your files. You can choose which "
"folders and what types of file data are indexed."
msgstr ""
"Filsökning hjälper dig att snabbt lokalisera alla dina filer baserat på "
"innehåll."

#: ui/main.qml:109
#, fuzzy, kde-format
#| msgid "Disable indexing"
msgctxt "@title:group"
msgid "File indexing:"
msgstr "Inaktivera indexering"

#: ui/main.qml:110
#, kde-format
msgctxt "@action:check"
msgid "Enabled"
msgstr ""

#: ui/main.qml:124
#, kde-format
msgctxt "@label indexing status"
msgid "Status:"
msgstr ""

#: ui/main.qml:128
#, fuzzy, kde-format
#| msgid "Status: %1, %2% complete"
msgctxt "State and a percentage of progress"
msgid "%1, %2% complete"
msgstr "Status: %1, %2% färdiga"

#: ui/main.qml:142
#, fuzzy, kde-format
#| msgid "Currently indexing: %1"
msgctxt "@label file currently being indexed"
msgid "Currently indexing:"
msgstr "Indexerar för närvarande: %1"

#: ui/main.qml:153
#, fuzzy, kde-kuit-format
#| msgid "Currently indexing: %1"
msgctxt "@info Currently Indexing"
msgid "<filename>%1</filename>"
msgstr "Indexerar för närvarande: %1"

#: ui/main.qml:169
#, kde-format
msgctxt "@title:group"
msgid "Data to index:"
msgstr ""

#: ui/main.qml:171
#, kde-format
msgid "File names and contents"
msgstr ""

#: ui/main.qml:184
#, kde-format
msgid "File names only"
msgstr ""

#: ui/main.qml:203
#, fuzzy, kde-format
#| msgid "Index hidden files and folders"
msgid "Hidden files and folders"
msgstr "Indexera dolda filer och kataloger"

#: ui/main.qml:229
#, kde-format
msgctxt "@title:table Locations to include or exclude from indexing"
msgid "Locations"
msgstr ""

#: ui/main.qml:232
#, fuzzy, kde-format
#| msgid "Start indexing a folder…"
msgctxt "@action:button"
msgid "Start Indexing a Folder…"
msgstr "Starta indexering av en katalog…"

#: ui/main.qml:240
#, fuzzy, kde-format
#| msgid "Stop indexing a folder…"
msgctxt "@action:button"
msgid "Stop Indexing a Folder…"
msgstr "Stoppa indexering av en katalog…"

#: ui/main.qml:300
#, kde-format
msgid "Not indexed"
msgstr "Inte indexerad"

#: ui/main.qml:301
#, kde-format
msgid "Indexed"
msgstr "Indexerad"

#: ui/main.qml:331
#, kde-format
msgid "Delete entry"
msgstr "Ta bort post"

#: ui/main.qml:346
#, kde-format
msgid "Select a folder to include"
msgstr "Välj en katalog att inkludera"

#: ui/main.qml:346
#, kde-format
msgid "Select a folder to exclude"
msgstr "Välj en katalog att undanta"

#~ msgid "Enable File Search"
#~ msgstr "Aktivera filsökning"

#~ msgid "Also index file content"
#~ msgstr "Indexera också filinnehåll"

#~ msgid "Folder specific configuration:"
#~ msgstr "Katalogspecifik inställning:"

#~ msgid ""
#~ "This module lets you configure the file indexer and search functionality."
#~ msgstr "Modulen låter dig anpassa filindexering och sökfunktioner."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "File Search"
#~ msgstr "Filsökning"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#~ msgid "Add folder configuration…"
#~ msgstr "Lägg till kataloginställning…"

#~ msgid "%1 is included."
#~ msgstr "%1 är inkluderad."

#~ msgid "%1 is excluded."
#~ msgstr "%1 är exkluderad."

#~ msgid "Do not search in these locations:"
#~ msgstr "Sök inte på följande platser:"
